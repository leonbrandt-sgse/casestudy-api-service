# sgse-casestudy-api-service

## Environment

There are no defaults. Every variable has to be set.

Variable | Description
--- | ---
`LOGLEVEL` | npm-loglevel
`DB_CONNECTION_URL` | URL to MongoDB
`LISTEN_PORT` | Port to expose the API to
`PROBE_LIVENESS_PORT` | Port to expose for healthcheck (liveness)
`PROBE_READINESS_PORT` | Port to expose for healthcheck (readiness)
