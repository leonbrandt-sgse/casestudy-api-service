import logger from "./logger";
import { EventEmitter } from "events";
import { Probe } from "@leonbrandt/sgse-casestudy-commons";

import { recordService } from "@leonbrandt/sgse-casestudy-commons";

import express from "express";
import http from "http";
import cors from "cors";
import bodyParser from "body-parser";

import { router } from "./routes";

export class Service extends EventEmitter {

	private _app: express.Application;
	private _server: http.Server;

	private _livenessPort: number;
	private _readinessPort: number;

	private _livenessProbe: Probe;
	private _readinessProbe: Probe;

	get app(): express.Application { return this._app; }

	constructor(livenessPort: number, readinessPort: number) {
		super();

		this._livenessPort = livenessPort;
		this._readinessPort = readinessPort;

		this._livenessProbe = new Probe(this._livenessPort);
		this._readinessProbe = new Probe(this._readinessPort);

		this._livenessProbe.on("error", (error: any) => {
			this.emit("probeerror", error);
		});

		this._readinessProbe.on("error", (error: any) => {
			this.emit("probeerror", error);
		});
	}

	public async start(): Promise<void> {
		logger.info("Starting service...");

		logger.debug("Liveness probe: set up");
		this._livenessProbe.setUp();

		try {
			await this.connect(process.env.DB_CONNECTION_URL);
		}
		catch(e) {
			logger.error("Error while connecting to database. Stopping");
			await this.stop();
			return;
		}

		try {
			await this.listen(+process.env.LISTEN_PORT);
		}
		catch(e) {
			logger.error("Error while starting webservice. Stopping");
			await this.stop();
			return;
		}

		logger.debug("Readyness probe: set up");
		this._readinessProbe.setUp();

		this.emit("ready");
	}

	private async connect(dbConnectionUrl: string): Promise<void> {
		logger.info(`Connecting to database... (${dbConnectionUrl})`);

		await recordService.connect(dbConnectionUrl);

		logger.info("Connected");
	}

	private async listen(port: number): Promise<void> {
		this._app = express();
		this._server = http.createServer(this._app);

		this._app.use(cors());
		this._app.use(bodyParser.json());
		this._app.use(bodyParser.urlencoded({ extended: false }));

		this._app.use((req: express.Request, res: express.Response, next: () => any) => {
			logger.http("> " + req.ip + " " + req.method + " " + req.originalUrl);
			next();
		});

		this._app.use("", router);

		try {
			logger.info("Starting webservice...");

			if(process.env.NODE_ENV === "production") {
				this._server.listen(port);

				logger.info("Started (listening to port " + port + ")");
			}
			else {
				logger.warn(`Not exposing a port, because environment-variable NODE_ENV is "${process.env.NODE_ENV}" (has to be "production")`);
			}
		}
		catch(e) {
			logger.error("Could not start webservice");
			logger.debug(e);
			throw e;
		}
	}

	public async stop(): Promise<void> {
		logger.info("Stopping service...");

		logger.debug("Readyness probe: set down");
		this._readinessProbe.setDown();

		if(this._server) {
			try {
				this._server.close();
				logger.info("Closed Webservice");
			}
			catch(e) {
				logger.error("Error while closing Webservice");
				logger.debug(e);
			}
		}

		logger.debug("Liveness probe: set down");
		this._livenessProbe.setDown();

		this.emit("stopped");
	}

}
