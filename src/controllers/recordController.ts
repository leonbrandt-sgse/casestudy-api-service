import { IRecord, recordService } from "@leonbrandt/sgse-casestudy-commons";
import * as express from "express";
import * as validator from "express-validator";

export class RecordController {

	public static createValidator = [
		validator.check("content")
			.exists().withMessage("CONTENT_REQUIRED"),
	];

	public static async create(req: express.Request, res: express.Response, next: () => any): Promise<void> {
		const record: IRecord = await recordService.create(req.body);

		res.status(200).json(record);
	}

	public static async retrieve(req: express.Request, res: express.Response, next: () => any): Promise<void> {
		const records: IRecord[] = await recordService.retrieve();

		res.status(200).json({ records });
	}

}
