import * as express from "express";
import * as validator from "express-validator";
import { validationResult } from "express-validator";

import { RecordController } from "./controllers/recordController";

export type AsyncMiddleware = (req: express.Request, res: express.Response, next: () => any) => Promise<void>;

export function showError(req: express.Request, res: express.Response, next: () => any) {
	let errors = validationResult(req);
	if(!errors.isEmpty()) return res.status(400).json(errors.array());
	return next();
}

const router: express.Router = express.Router();

router.get("/health", (req: express.Request, res: express.Response, next: () => any) => {
	return res.status(200).send();
});

router.put("/record", RecordController.createValidator, showError, RecordController.create);
router.get("/records", RecordController.retrieve);

export { router };
