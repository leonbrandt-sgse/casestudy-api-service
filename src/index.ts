require("dotenv").config();

import logger from "./logger";
import { Service } from "./service";

export const instance: Service = new Service(+process.env.PROBE_LIVENESS_PORT, +process.env.PROBE_READINESS_PORT);

process.on("SIGTERM", instance.stop);

instance.on("probeerror", (error: any) => {
	logger.error(error);
	process.exit(-1);
});

instance.on("ready", () => {
	logger.info("IDLE...");
});

if(process.env.NODE_ENV === "production") instance.start();
